package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ex6springv2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ex6springv2Application.class, args);
	}
}