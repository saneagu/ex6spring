package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class UploadController {

    @PostMapping("/")
    public void doUpload(@RequestParam ("upload") MultipartFile fileFromForm) {

        StorageService ss = new StorageService();
        ss.store(fileFromForm);
    }

    @GetMapping("/")
    public String basicPage(){
        return "index";
    }
}
